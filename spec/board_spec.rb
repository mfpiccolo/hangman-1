require 'spec_helper'

describe Board do 
  context '#display' do 
    it 'should return a new board' do
      board = Board.new
      board.display(0).should eq "    ________________\n    /\\               |\n    \\/               |\n     |               |\n  -------            |\n     |               |\n     |               |\n    /\\               |\n   /  \\              |\n               ______|_____\n"
    end
  end
end