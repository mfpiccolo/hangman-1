require 'spec_helper'

describe Word do
  # context '#to_s' do
  #   it 'should return the word as a string' do
  #     word = Word.new
  #     ['player', 'winner', 'pencil', 'screen', 'spicey', 'island', 'exotic'].should include word.to_s
  #   end
  # end

  context '#contains?' do
    it 'should check if a word includes a letter' do
      word = Word.new
      word.stub(:to_string).and_return("blabla")
      word.contains?('m').should eq false
    end
  end

  context '#contains?' do
    it 'should check if a word includes a letter' do
      word = Word.new
      word.stub(:to_s).and_return("morning")
      word.contains?('r').should eq true
    end
  end
end

