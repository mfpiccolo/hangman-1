require 'spec_helper'

describe Game do 
  context '#over?' do 
    it 'checks if game is over' do
      game = Game.new
      game.over?.should eq false
    end

    it 'checks if player has lost (failed_attempts >= 6)' do
      word = double
      Word.stub(:new).and_return(word)
      word.stub(:to_string).and_return('myword')
      word.stub(:contains?).with('x').and_return(false)
      game = Game.new
      6.times { game.letter_check("x") }
      game.over?.should eq true
    end

    it 'checks if player has won' do
      word = double
      Word.stub(:new).and_return(word)
      word.stub(:to_string).and_return('zzyyxx')
      word.stub(:contains?).and_return(true)
      game = Game.new
      game.letter_check("x")
      game.letter_check("y")
      game.letter_check("z")
      game.over?.should eq true
    end
  end

  context '#end' do
    it "prints 'you lost' if player has made 6 or more incorrect guesses" do
      word = double
      Word.stub(:new).and_return(word)
      word.stub(:contains?).with('x').and_return(false)
      game = Game.new
      6.times { game.letter_check("x") }
      game.end.should eq 'You lost!'
    end

    it 'prits you won if player guessed all the letters' do
      word = double
      Word.stub(:new).and_return(word)
      word.stub(:contains?).with('x').and_return(true)
      word.stub(:to_string).and_return('xxxxxx')
      game = Game.new
      6.times { game.letter_check("x") }
      game.over?
      game.end.should eq 'You win!'
    end
  end

  context '#letter_check(letter)' do
    it 'checking if #contains? is called on word' do
      word = double
      Word.stub(:new).and_return(word)
      game = Game.new
      word.stub(:contains?).with('m').and_return(true)
      game.letter_check('m').should eq 'm'
    end
  end

  # context '#failed_attempts' do 
  #   it 'counts the number of failed_attempts' do 
  #     game = Game.new
  #     game.failed_attempts.should eq 0
  #   end

  #   it 'counts the number of failed_attempts' do 
  #     game = Game.new
  #     game.letter_check('q')
  #     game.failed_attempts.should eq 1
  #   end
  # end

  context '#show_blanks' do
    it 'should show the word as blanks substituted by any correct guesses' do
      game = Game.new
      game.show_blanks.should eq "_ _ _ _ _ _"
    end

    it 'should show the blanks with a "c" as a correct guess.' do
      word = double
      Word.stub(:new).and_return(word)
      word.stub(:to_string).and_return('places')
      word.stub(:contains?).with('c').and_return(true)
      game = Game.new
      game.letter_check("c")
      game.show_blanks.should eq "_ _ _ c _ _"
    end
  end

  context '#show_incorrect_guesses' do 
    it 'should return a string containing the incorrect letters that have been guessed' do
      game = Game.new
      game.show_incorrect_guesses.should eq ''
    end

    it 'should list as a string the incorrect letters that were guessed' do
      word = double
      Word.stub(:new).and_return(word)
      word.stub(:contains?).with('x').and_return(false)
      game = Game.new
      game.letter_check("x")
      game.show_incorrect_guesses.should eq 'x'
    end
  end
end