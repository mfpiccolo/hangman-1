class Word
  
  def initialize
    @bank = ['player', 'winner', 'pencil', 'screen', 'spicey', 'island', 'exotic']
    p @word = @bank[rand(0..@bank.length - 1)]
  end

  def to_string
    @word.to_s
  end

  def contains?(letter)
    self.to_string.include? letter
  end

end