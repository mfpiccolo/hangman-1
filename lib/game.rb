class Game
  attr_reader :failed_attempts
  attr_reader :board

  def initialize
    @game_word = Word.new
    @board = Board.new
    @incorrect_guesses = []
    @correct_guesses = []
    @failed_attempts = 0
  end

  def over?
    if @failed_attempts >= 6
      @lose = true
    elsif self.show_blanks.include? '_'
      @win = false
    else
      @win = true
    end
  end

  def end
    if @lose == true
      'You lost!'
    elsif @win == true
      "You win!"
    else
      "You lost!"
    end
  end

  def letter_check(letter)
    if @game_word.contains?(letter)
      @correct_guesses << letter
      letter
    else
      @failed_attempts += 1
      @incorrect_guesses << letter
    end
  end

  def show_incorrect_guesses
    @incorrect_guesses.join(", ")
  end

  def show_blanks
    (@game_word.to_string.split("").map do |letter|
      if @correct_guesses.include?(letter)
        letter
      else
        '_'
      end
    end).join(" ")
    # @word.to_s.split("").each_with_index  { |letter, index| @blanks.insert((index * 2), "_") }
    # @blanks.compact!
  end
end

