class Board

def display(failed_attempts)
  if failed_attempts == 0
    return "    ________________\n    /\\               |\n    \\/               |\n     |               |\n  -------            |\n     |               |\n     |               |\n    /\\               |\n   /  \\              |\n               ______|_____\n"
  else
    nil
  end
end



# "    ________________
#     /\\               |
#     \\/               |
#      |               |
#   -------            |
#      |               |
#      |               |
#     /\\               |
#    /  \\              |
#                ______|_____\n"
end