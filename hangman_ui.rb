require './lib/game'
require './lib/word'
require './lib/board'



game = Game.new
puts "Welcome to hangman."
#first turn is different?
until game.over?
  puts "You have made #{game.failed_attempts} incorrect guesses."
  print game.board.display(game.failed_attempts)
  print game.show_blanks
  game.show_incorrect_guesses
  puts "Guess another letter:"
  guess = gets.chomp
  game.letter_check(guess)
end

if game.over?
  puts game.end
end